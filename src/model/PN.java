package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PN extends Ordination {
    private final double antalEnheder;
    private List<LocalDate> datoForGivetDosis;

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
        super(startDen, slutDen);
        this.antalEnheder = antalEnheder;
        this.datoForGivetDosis = new ArrayList<>();
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen. Returnerer true,
     * hvis givesDen er inden for ordinationens gyldighedsperiode, og datoen huskes.
     * Returnerer false ellers og datoen givesDen ignoreres.
     */
    public boolean givDosis(LocalDate givesDen) {
        if (givesDen.equals(getStartDen()) || givesDen.equals(getSlutDen())
                || (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()))) {
            datoForGivetDosis.add(givesDen);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returnerer antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
        return datoForGivetDosis.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public double samletDosis() {
        return datoForGivetDosis.size() * antalEnheder;
    }

    @Override
    public double doegnDosis() {
        if (this.datoForGivetDosis.size() != 0) {
            LocalDate førsteDosis = this.datoForGivetDosis.get(0);
            LocalDate sidsteDosis = this.datoForGivetDosis.get(datoForGivetDosis.size() - 1);

            for (LocalDate ld : this.datoForGivetDosis) {
                if (ld.isBefore(førsteDosis)) {
                    ld = førsteDosis;
                }
                if (ld.isAfter(sidsteDosis)) {
                    ld = sidsteDosis;
                }
            }
            long døgn = ChronoUnit.DAYS.between(førsteDosis, sidsteDosis);
            return samletDosis() / døgn;
        } else {
            return 0;
        }
    }

    @Override
    public String getType() {
        return "Type: PN";
    }
}
