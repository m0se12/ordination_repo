package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    private ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }

    public void removeDosis(Dosis dosis) {
        this.doser.remove(dosis);
    }

    /** Pre: antal >= 0. */
    public void opretDosis(LocalTime tid, double antal) {
        assert antal >= 0;
        Dosis dosis = new Dosis(tid, antal);
        this.doser.add(dosis);
    }

    @Override
    public double samletDosis() {
        int sum = 0;
        for (Dosis d : doser) {
            sum += d.getAntal();
        }
        return sum * antalDage();
    }

    @Override
    public double doegnDosis() {
        return samletDosis() / antalDage();

    }

    @Override
    public String getType() {
        return "Type: DagligSkaev ";
    }

}
