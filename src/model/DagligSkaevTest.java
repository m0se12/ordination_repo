package model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {
    private LocalDate date1, date2, date3, date4, date5, date6;

    @Before
    public void setup() {
        date1 = LocalDate.parse("2018-03-01");
        date2 = LocalDate.parse("2018-03-07");
        date3 = LocalDate.parse("2018-02-20");
        date4 = LocalDate.parse("2018-02-28");
        date5 = LocalDate.parse("2018-04-06");
        date6 = LocalDate.parse("2018-04-10");
    }

    @Test
    public void testOpretDosis1() {
        DagligSkaev ds = new DagligSkaev(date1, date2);
        ds.opretDosis(LocalTime.parse("10:00"), 1);
        Dosis d = ds.getDoser().get(0);

        LocalTime tid = d.getTid();
        double antal = d.getAntal();
        int length = ds.getDoser().size();

        assertEquals(LocalTime.parse("10:00"), tid);
        assertEquals(1, antal, 0.001);
        assertEquals(1, length);

    }

    @Test
    public void testOpretDosis2() {
        DagligSkaev ds = new DagligSkaev(date1, date2);
        ds.opretDosis(LocalTime.parse("14:00"), 3);
        Dosis d = ds.getDoser().get(0);

        LocalTime tid = d.getTid();
        double antal = d.getAntal();
        int length = ds.getDoser().size();

        assertEquals(LocalTime.parse("14:00"), tid);
        assertEquals(3, antal, 0.001);
        assertEquals(1, length);

    }

    @Test
    public void doegnDosis1() {
        DagligSkaev ds = new DagligSkaev(date1, date2);
        ds.opretDosis(LocalTime.parse("14:00"), 2);
        ds.opretDosis(LocalTime.parse("21:00"), 4);
        double resultat = ds.doegnDosis();
        assertEquals(6, resultat, 0.001);
    }

    @Test
    public void doegnDosis2() {
        DagligSkaev ds = new DagligSkaev(date3, date4);
        ds.opretDosis(LocalTime.parse("14:00"), 1);
        ds.opretDosis(LocalTime.parse("21:00"), 3);
        double resultat = ds.doegnDosis();
        assertEquals(4, resultat, 0.001);
    }

    @Test
    public void samletDosis() {
        DagligSkaev ds = new DagligSkaev(date5, date6);
        ds.opretDosis(LocalTime.parse("09:00"), 2);
        ds.opretDosis(LocalTime.parse("17:00"), 2);
        long day = ChronoUnit.DAYS.between(date5, date6) + 1;
        double resultat = ds.doegnDosis();
        int sum = (int) (day * resultat);
        assertEquals(20, sum, 0.001);
    }

}
