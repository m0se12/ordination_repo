package model;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {
    private LocalDate date1;
    private LocalDate date2;
    private LocalDate date3;
    private LocalDate date4;
    private LocalDate date5;
    private LocalDate date6;
    private LocalDate date7;
    private LocalDate date8;
    private LocalDate date9;

    @Before
    public void setUp() throws Exception {
        date1 = LocalDate.parse("2018-02-20");
        date2 = LocalDate.parse("2018-02-28");
        date3 = LocalDate.parse("2018-02-22");
        date4 = LocalDate.parse("2018-02-23");
        date5 = LocalDate.parse("2018-02-24");
        date6 = LocalDate.parse("2018-02-25");
        date7 = LocalDate.parse("2018-02-26");
        date8 = LocalDate.parse("2018-02-27");
        date9 = LocalDate.parse("2018-03-01");
    }

    @Test
    public void testGivDosis() {
        PN benis = new PN(date1, date2, 3);
        benis.givDosis(date3);
        int resultat = benis.getAntalGangeGivet();
        int expected = 1;
        assertEquals(resultat, expected);
    }

    @Test
    public void testGivDosis2() {
        PN benis = new PN(date1, date2, 3);
        benis.givDosis(date9);
        int resultat = benis.getAntalGangeGivet();
        int expected = 0;
        assertEquals(resultat, expected);
    }

    @Test
    public void testGetAntalGangeGivet() {
        PN benis = new PN(date1, date2, 3);
        benis.givDosis(date3);
        benis.givDosis(date5);
        benis.givDosis(date8);
        int resultat = benis.getAntalGangeGivet();
        int expected = 3;
        assertEquals(resultat, expected);
    }

    @Test
    public void testDoegnDosis() {
        PN benis = new PN(date1, date2, 3);
        benis.givDosis(date3);
        benis.givDosis(date4);
        benis.givDosis(date5);
        benis.givDosis(date6);
        benis.givDosis(date7);
        benis.givDosis(date8);
        double resultat = benis.doegnDosis();
        double expected = 3.6;
        assertEquals(resultat, expected, 0.01);
    }

    @Test
    public void testSamletDosis() {
        PN benis = new PN(date1, date2, 3);
        benis.givDosis(date3);
        benis.givDosis(date4);
        benis.givDosis(date5);
        benis.givDosis(date6);
        benis.givDosis(date7);
        benis.givDosis(date8);
        double resultat = benis.samletDosis();
        double expected = 18;
        assertEquals(resultat, expected, 0.01);
    }
}
