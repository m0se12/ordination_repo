package model;

import java.time.LocalTime;

public class Dosis {
    private final LocalTime tid;
    private final double antal;
    private DagligSkaev dagligSkaev;
    private DagligFast dagligFast;
    // Negativt antal betyder, at antal ikke er sat.

    public DagligSkaev getDagligSkaev() {
        return dagligSkaev;
    }

    void setDagligSkaev(DagligSkaev dagligSkaev) {
        this.dagligSkaev = dagligSkaev;
    }

    public DagligFast getDagligFast() {
        return dagligFast;
    }

    void setDagligFast(DagligFast dagligFast) {
        this.dagligFast = dagligFast;
    }

    public Dosis(LocalTime tid, double antal) {
        assert tid != null;
        this.tid = tid;
        this.antal = antal;
    }

    public double getAntal() {
        return antal;
    }

    public LocalTime getTid() {
        return tid;
    }

    @Override
    public String toString() {
        return "Kl: " + tid + "   antal:  " + antal;
    }
}
