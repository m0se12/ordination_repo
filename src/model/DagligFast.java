package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
    private Dosis[] doser = new Dosis[4];

    public DagligFast(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    @Override
    public double samletDosis() {
        int sum = 0;
        for (int i = 0; i < 4; i++) {
            if (doser[i] != null) {
                sum += doser[i].getAntal();
            }
        }
        return sum * antalDage();
    }

    @Override
    public double doegnDosis() {
        return samletDosis() / antalDage();
    }

    @Override
    public String getType() {
        return "Daglig Fast";
    }

    public Dosis[] getDoser() {
        return doser;
    }

    public void setDoser(Dosis[] doser) {
        this.doser = doser;
    }

    public void createDosis(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
        if (morgenAntal > 0) {
            doser[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
        }
        if (middagAntal > 0) {
            doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
        }
        if (aftenAntal > 0) {
            doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
        }
        if (natAntal > 0) {
            doser[3] = new Dosis(LocalTime.of(21, 0), natAntal);
        }
    }
}
