package model;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {
    private LocalDate date1, date2, date3, date4;

    @Before
    public void setUp() throws Exception {
        date1 = LocalDate.parse("2018-03-01");
        date2 = LocalDate.parse("2018-03-07");
        date3 = LocalDate.parse("2018-03-10");
        date4 = LocalDate.parse("2018-04-01");
    }

    @Test
    public void testSamletDosis() {
        DagligFast d = new DagligFast(date1, date2);
        Dosis[] doser = new Dosis[4];
        doser[0] = new Dosis(LocalTime.of(6, 0), 2);
        doser[2] = new Dosis(LocalTime.of(18, 0), 3);
        doser[3] = new Dosis(LocalTime.of(21, 0), 1);
        d.setDoser(doser);
        double resultat = d.samletDosis();
        assertEquals(42, resultat, 0.001);
    }

    @Test
    public void testSamletDosis1() {
        DagligFast d = new DagligFast(date1, date3);
        Dosis[] doser = new Dosis[4];
        doser[1] = new Dosis(LocalTime.of(12, 0), 1);
        d.setDoser(doser);
        double resultat = d.samletDosis();
        assertEquals(10, resultat, 0.001);
    }

    @Test
    public void testDoegnDosis() {
        DagligFast d = new DagligFast(date1, date2);
        Dosis[] doser = new Dosis[4];
        doser[1] = new Dosis(LocalTime.of(12, 0), 4);
        doser[3] = new Dosis(LocalTime.of(21, 0), 1);
        d.setDoser(doser);
        double resultat = d.doegnDosis();
        assertEquals(5, resultat, 0.001);
    }

    @Test
    public void testGetType() {
        DagligFast df = new DagligFast(date1, date2);
        String resultat = df.getType();
        String expected = "Daglig Fast";
        assertEquals(expected, resultat);
    }

    @Test
    public void testGetDoser() {
        DagligFast df = new DagligFast(date1, date2);
        Dosis[] doser = new Dosis[4];
        doser[0] = new Dosis(LocalTime.of(6, 0), 3);
        doser[2] = new Dosis(LocalTime.of(18, 0), 4);
        df.setDoser(doser);
        Dosis[] resultat = df.getDoser();
        assertArrayEquals(doser, resultat);
    }

    @Test
    public void testCreateDosis() {
        DagligFast df = new DagligFast(date1, date2);
        double morgenAntal = 2;
        double middagAntal = -1;
        double aftenAntal = -1;
        double natAntal = -1;
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        Dosis[] resultat = df.getDoser();
        assertEquals(morgenAntal, resultat[0].getAntal(), 0.01);
        assertNull(resultat[1]);
        assertNull(resultat[2]);
        assertNull(resultat[3]);
    }

    @Test
    public void testCreateDosis1() {
        DagligFast df = new DagligFast(date1, date2);
        double morgenAntal = -1;
        double middagAntal = 2;
        double aftenAntal = -1;
        double natAntal = -1;
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        Dosis[] resultat = df.getDoser();
        assertEquals(middagAntal, resultat[1].getAntal(), 0.01);
        assertNull(resultat[0]);
        assertNull(resultat[2]);
        assertNull(resultat[3]);
    }

    @Test
    public void testCreateDosis2() {
        DagligFast df = new DagligFast(date1, date2);
        double morgenAntal = -1;
        double middagAntal = -1;
        double aftenAntal = 2;
        double natAntal = -1;
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        Dosis[] resultat = df.getDoser();
        assertEquals(aftenAntal, resultat[2].getAntal(), 0.01);
        assertNull(resultat[1]);
        assertNull(resultat[0]);
        assertNull(resultat[3]);
    }

    @Test
    public void testCreateDosis3() {
        DagligFast df = new DagligFast(date1, date2);
        double morgenAntal = -1;
        double middagAntal = -1;
        double aftenAntal = -1;
        double natAntal = 2;
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        Dosis[] resultat = df.getDoser();
        assertEquals(natAntal, resultat[3].getAntal(), 0.01);
        assertNull(resultat[1]);
        assertNull(resultat[2]);
        assertNull(resultat[0]);
    }

    @Test
    public void testCreateDosis4() {
        DagligFast df = new DagligFast(date1, date2);
        double morgenAntal = 2;
        double middagAntal = 2;
        double aftenAntal = 2;
        double natAntal = 2;
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        Dosis[] resultat = df.getDoser();
        assertEquals(morgenAntal, resultat[0].getAntal(), 0.01);
        assertEquals(middagAntal, resultat[1].getAntal(), 0.01);
        assertEquals(aftenAntal, resultat[2].getAntal(), 0.01);
        assertEquals(natAntal, resultat[3].getAntal(), 0.01);
    }

    @Test
    public void testGetStartDen() {
        DagligFast df = new DagligFast(date1, date4);
        LocalDate resultat = df.getStartDen();
        LocalDate expected = LocalDate.parse("2018-03-01");
        assertEquals(resultat, expected);
    }

    @Test
    public void testGetSlutDen() {
        DagligFast df = new DagligFast(date1, date4);
        LocalDate resultat = df.getSlutDen();
        LocalDate expected = LocalDate.parse("2018-04-01");
        assertEquals(resultat, expected);
    }

    @Test
    public void testGetLaegemiddel() {
        DagligFast df = new DagligFast(date1, date2);
        Laegemiddel paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        df.setLeagemiddel(paracetamol);
        Laegemiddel expected = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(expected.getNavn(), df.getLaegemiddel().getNavn());
        assertEquals(expected.getEnhedPrKgPrDoegnLet(), df.getLaegemiddel().getEnhedPrKgPrDoegnLet(), 0.01);
        assertEquals(expected.getEnhedPrKgPrDoegnNormal(), df.getLaegemiddel().getEnhedPrKgPrDoegnNormal(), 0.01);
        assertEquals(expected.getEnhedPrKgPrDoegnTung(), df.getLaegemiddel().getEnhedPrKgPrDoegnTung(), 0.01);
        assertEquals(expected.getEnhed(), df.getLaegemiddel().getEnhed());
    }

}
