package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.DagligFast;
import model.DagligSkaev;
import model.Dosis;
import model.Laegemiddel;
import model.PN;
import model.Patient;
import storage.Storage;

public class ServiceTest {
    private Service service;
    private Patient jane, finn, hans, ulla, ib;
    private Laegemiddel acetylsalicylsyre, paracetamol, fucidin, methotrexat;
    private Storage storage;

    @Before
    public void setUp() throws Exception {
        storage = new Storage();
        service = new Service(storage);

        jane = service.opretPatient("121256-0512", "Jane Jensen", 63.4);
        finn = service.opretPatient("070985-1153", "Finn Madsen", 83.2);
        hans = service.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        ulla = service.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        ib = service.opretPatient("090149-2529", "Ib Hansen", 87.7);

        acetylsalicylsyre = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        paracetamol = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        fucidin = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        methotrexat = service.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

    }

    @Test
    public void testOpretPNOrdination() {
        LocalDate startDen = LocalDate.parse("2018-03-01");
        LocalDate slutDen = LocalDate.parse("2018-03-10");
        PN pn = service.opretPNOrdination(startDen, slutDen, hans, paracetamol, 2);

        assertEquals(startDen, pn.getStartDen());
        assertEquals(slutDen, pn.getSlutDen());
        assertEquals(paracetamol, pn.getLaegemiddel());
        assertEquals(true, hans.getOrdinationer().contains(pn));
        assertEquals(2, pn.getAntalEnheder(), 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination1() {
        LocalDate startDen = LocalDate.parse("2018-04-01");
        LocalDate slutDen = LocalDate.parse("2018-04-10");
        PN pn = service.opretPNOrdination(startDen, slutDen, finn, acetylsalicylsyre, -1);

        assertEquals(startDen, pn.getStartDen());
        assertEquals(slutDen, pn.getSlutDen());
        assertEquals(acetylsalicylsyre, pn.getLaegemiddel());
        assertEquals(true, hans.getOrdinationer().contains(pn));
        assertEquals(2, pn.getAntalEnheder(), 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination2() {
        LocalDate startDen = LocalDate.parse("2018-03-01");
        LocalDate slutDen = LocalDate.parse("2018-01-28");
        PN pn = service.opretPNOrdination(startDen, slutDen, hans, paracetamol, 2);

        assertEquals(startDen, pn.getStartDen());
        assertEquals(slutDen, pn.getSlutDen());
        assertEquals(paracetamol, pn.getLaegemiddel());
        assertEquals(true, hans.getOrdinationer().contains(pn));
        assertEquals(2, pn.getAntalEnheder(), 0.01);
    }

    @Test
    public void testOpretDagligFastOrdination() {
        LocalDate startDen = LocalDate.parse("2018-02-15");
        LocalDate slutDen = LocalDate.parse("2018-02-20");
        DagligFast df = service.opretDagligFastOrdination(startDen, slutDen, jane, methotrexat, 1, 0, 2, 1);

        assertEquals(startDen, df.getStartDen());
        assertEquals(slutDen, df.getSlutDen());
        assertEquals(methotrexat, df.getLaegemiddel());
        assertEquals(true, jane.getOrdinationer().contains(df));
        assertEquals(1.0, df.getDoser()[0].getAntal(), 0.01);
        assertNull(df.getDoser()[1]);
        assertEquals(2.0, df.getDoser()[2].getAntal(), 0.01);
        assertEquals(1.0, df.getDoser()[3].getAntal(), 0.01);
    }

    @Test
    public void testOpretDagligFastOrdination1() {
        LocalDate startDen = LocalDate.parse("2018-03-15");
        LocalDate slutDen = LocalDate.parse("2018-03-20");
        DagligFast df = service.opretDagligFastOrdination(startDen, slutDen, ib, methotrexat, 4, 0, 0, 1);

        assertEquals(startDen, df.getStartDen());
        assertEquals(slutDen, df.getSlutDen());
        assertEquals(methotrexat, df.getLaegemiddel());
        assertEquals(true, ib.getOrdinationer().contains(df));
        assertEquals(4.0, df.getDoser()[0].getAntal(), 0.01);
        assertNull(df.getDoser()[1]);
        assertNull(df.getDoser()[2]);
        assertEquals(1.0, df.getDoser()[3].getAntal(), 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination2() {
        LocalDate startDen = LocalDate.parse("2018-03-15");
        LocalDate slutDen = LocalDate.parse("2018-03-10");
        DagligFast df = service.opretDagligFastOrdination(startDen, slutDen, ib, methotrexat, 4, 0, 0, 1);

        assertEquals(startDen, df.getStartDen());
        assertEquals(slutDen, df.getSlutDen());
        assertEquals(methotrexat, df.getLaegemiddel());
        assertEquals(true, ib.getOrdinationer().contains(df));
        assertEquals(4.0, df.getDoser()[0].getAntal(), 0.01);
        assertNull(df.getDoser()[1]);
        assertNull(df.getDoser()[2]);
        assertEquals(1.0, df.getDoser()[3].getAntal(), 0.01);
    }

    @Test
    public void testOpretDagligSkaevOrdination() {
        LocalDate startDen = LocalDate.parse("2018-03-10");
        LocalDate slutDen = LocalDate.parse("2018-03-18");
        LocalTime[] tid = { LocalTime.parse("10:00") };
        double[] enheder = { 3 };
        DagligSkaev ds = service.opretDagligSkaevOrdination(startDen, slutDen, finn, fucidin, tid, enheder);

        LocalTime k1 = ds.getDoser().get(0).getTid();

        double a1 = ds.getDoser().get(0).getAntal();

        assertEquals(startDen, ds.getStartDen());
        assertEquals(slutDen, ds.getSlutDen());
        assertEquals(fucidin, ds.getLaegemiddel());
        assertEquals(true, finn.getOrdinationer().contains(ds));
        assertEquals(LocalTime.parse("10:00"), k1);
        assertEquals(3, a1, 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination1() {
        LocalDate startDen = LocalDate.parse("2018-03-16");
        LocalDate slutDen = LocalDate.parse("2018-03-20");
        LocalTime[] tid = { LocalTime.parse("17:00") };
        double[] enheder = { -1 };
        DagligSkaev ds = service.opretDagligSkaevOrdination(startDen, slutDen, ib, acetylsalicylsyre, tid, enheder);

        LocalTime k1 = ds.getDoser().get(0).getTid();

        double a1 = ds.getDoser().get(0).getAntal();

        assertEquals(startDen, ds.getStartDen());
        assertEquals(slutDen, ds.getSlutDen());
        assertEquals(acetylsalicylsyre, ds.getLaegemiddel());
        assertEquals(true, ib.getOrdinationer().contains(ds));
        assertEquals(LocalTime.parse("17:00"), k1);
        assertEquals(-1, a1, 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination2() {
        LocalDate startDen = LocalDate.parse("2018-03-21");
        LocalDate slutDen = LocalDate.parse("2018-03-20");
        LocalTime[] tid = { LocalTime.parse("17:00") };
        double[] enheder = {};
        DagligSkaev ds = service.opretDagligSkaevOrdination(startDen, slutDen, hans, paracetamol, tid, enheder);

        LocalTime k1 = ds.getDoser().get(0).getTid();

        double a1 = ds.getDoser().get(0).getAntal();

        assertEquals(startDen, ds.getStartDen());
        assertEquals(slutDen, ds.getSlutDen());
        assertEquals(acetylsalicylsyre, ds.getLaegemiddel());
        assertEquals(true, ib.getOrdinationer().contains(ds));
        assertEquals(LocalTime.parse("17:00"), k1);
        assertEquals(-1, a1, 0.01);
    }

    @Test
    public void testOrdinationPNAnvendt() {
        LocalDate startDen = LocalDate.parse("2015-01-01");
        LocalDate slutDen = LocalDate.parse("2015-01-12");
        PN pn = service.opretPNOrdination(startDen, slutDen, jane, paracetamol, 2);
        service.ordinationPNAnvendt(pn, LocalDate.parse("2015-01-10"));

        assertEquals(1, pn.getAntalGangeGivet());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendt1() {
        LocalDate startDen = LocalDate.parse("2015-01-01");
        LocalDate slutDen = LocalDate.parse("2015-01-12");
        PN pn = service.opretPNOrdination(startDen, slutDen, hans, paracetamol, 2);
        service.ordinationPNAnvendt(pn, LocalDate.parse("2015-01-14"));

        assertEquals(1, pn.getAntalGangeGivet());
    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel() {
        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), jane, paracetamol, 123);
        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), jane, paracetamol, 123);
        service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), finn, paracetamol, 2,
                -1, 1, -1);

        double resultat = service.antalOrdinationerPrVægtPrLægemiddel(50, 65, paracetamol);
        assertEquals(2, resultat, 0.01);

    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel1() {

        double resultat = service.antalOrdinationerPrVægtPrLægemiddel(50, 65, methotrexat);
        assertEquals(0, resultat, 0.01);

    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel2() {
        service.opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), hans, fucidin, 5);

        double resultat = service.antalOrdinationerPrVægtPrLægemiddel(65, 90, fucidin);
        assertEquals(1, resultat, 0.01);

    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel3() {

        double resultat = service.antalOrdinationerPrVægtPrLægemiddel(65, 90, acetylsalicylsyre);
        assertEquals(0, resultat, 0.01);

    }

}
